# git-p2p

_git-p2p_ is designed to enable effortless sharing of git repos/branches in a local setting. Collaboration in a personal setting should be as effortless as possible, and in no way should technology be the bottleneck.

__Status__: Initiation ... nothing to see here

# Goal

_git-p2p_ offers a p2p-like interaction for sharing repository/branch content in a local setting (same machine, local area network, etc.), where access to the internet is not guaranteed.

Using _git-p2p_ should be as easy a single command.

# Requirements

1. A new user without knowledge of _git-p2p_ must be able to find out in seconds how to start, i.e. only need to remember `git p2p`.
1. An existing user, a previously set up p2p configuration, should be usable immediately.
1. Most, if not all actions, should be issuable with _a single command_.
1. _URI_ as identifier to distinguish multiple different code-bases offered on the p2p network.
1. Non-persistence is primary use cases:
   - _git-p2p_ needs to be activated manually.
   - Information is only exposed for the duration of the session.

# Justification

There are plenty of git hosting sites:
1. requires internet connection,
1. requires set-up of a repository on such as server,
1. requires sharing of a certain URL,
1. (optionally) requires authn/authz to grant access.

Make git repository accessible through SSH, git-daemon or similar:
1. requires setting up an SSH server/git-daemon/...
1. (optionally) requires setting up authn/authz to grant access.

# Planning

1. Minimal version:
   - Define a remote that refers to `git-p2p` (protocol `p2p`)
   - Expose branch through common _git-p2p_ server.
   - Enable cloning/fetching/pushing from one repo to another through (single, shared) _git-p2p_ server.
   - Commands:
     - `git clone` - acquire repo/branch data through shared git-p2p server.
     - `git fetch` - acquire new repo/branch data through shared git-p2p server.
     - `git push` - offer repo/branch through shared git-p2p server.
2. ... extend from there ...

# Open issues

- How to handle multiple sources for single project?  
  _A lot of conflicting "master" reference commits would be available. How do we choose? (Choose latest by downloading commit chain from all and selecting the largest chain?)_

# Future

- IPC with _git-p2p_ server process.
- Network connectivity.
- Support multiple processes with IPC.
- Support network connectivity:
  - UDP-based communication
  - firewall hole-punching
- Authentication through shared symmetric key (password).
- More advanced forms of authentication.
- ...
- Extend to more exotic forms of local area networks, such as Bluetooth PAN.

# Notes

```
- _git-p2p_ tool that "hosts" a p2p git channel to which you can push your changes in order for them to be distributed over the local p2p populus. The current state is some common denominator of the available commits. Simply running the p2p-git server will connect you with everyone else in the same local area.
  - GOAL: you are working in any networked environment and want to collaborate with other people (e.g. colleagues). Which means:
    - Access to the internet is not guaranteed, or internet connection might be (too) slow.
    - You do not, at that moment, care about anything that happens on the internet. (The collaboration is local only.)
    - Availability of servers is not guaranteed.
    - No complicated setting up of systems, servers. You might not know how, you might not understand, you might not have permission, you might not have time.
    - You have limited control over (or understanding of) your firewall to set up your own git server daemon.
    - PREREQ: the infrastructure allows you to cooperate with other clients on the network.
  - REQUIREMENTS: p2p-connectivity
    - No complicated set-up of servers, i.e. single command should initiate the p2p daemon / look up the other peers / set up everything to start working.
    - Automatically look up other peers.
    - Punch hole through firewall if needed. (UDP i.s.o. TCP connection should help a lot here, though only speculation at this point.)
    - "push" promotes new HEAD commit for branch, p2p will pick up and promote on the p2p-network.
    - "fetch" retrieves other promoted commits.
    - Need to determine leading commit when branching occurs between peers: first commit seen / largest supporter base / first in lexicographical order / ...
  - INCREMENTS:
    - Given fully-configured network, perform git data transactions.
    - Establish DHT and peer knowledge of all connected peers.
    - Introduce basic networking to establish p2p over TCP or (preferably) UDP.
    - Introduce firewall-piercing mechanisms to ensure that p2p works in many settings as far as infrastructure allows.
    - Add authentication (PAKE/ZK), such that only users with right password are allowed access to the repository.
    - Evaluate security-properties given the set-up of p2p network and running server process.
  - NOTES:
    - Redundancy: what happens if peer suddenly leaves/drops and not all data was fetched yet. (Maybe configure server to function as mirror by automatically fetching on new announces.)
    - Have `git push <p2p-origin>` automatically start the server and connect to peer-network, mark `<branch>` as available on peer-network.
    - Have `git fetch <p2p-origin>` automatically start the server, connect to peer-network and query for defined branch such that sources can be acquired.
    - Have `git-p2p` command to configure run-time and static configuration for operating mode of the p2p daemon.
    - Have `git-p2p` to kill daemon in case of issues.
  - REFERENCES:
    - https://git-scm.com/docs/gitremote-helpers (newer git)
    - https://git-scm.com/docs/git-remote-helpers (older git)
    - https://git-scm.com/book/en/v2/Git-Internals-Transfer-Protocols
    - https://github.com/perlin-network/noise
    - https://github.com/xtaci/kcp-go
  - Use protocol to "redirect" to p2p networks.
  - Use origin URL as identifier/distinguisher for repository.
  - Use zero-knowledge protocol to verify if provider and consumer agree on secret as way of authentication.
  - p2p knowledge: `<origin-identifier>` → `<branch>:<latest-commit>`
  - Establish shared state and communication with other p2p participants.
  - Leading commit: largest chain is leading commit,
  - What about permissions management? (Delegate purely to the zk authn?)
  - Tech:
    - [xtaci/kcp-go](https://github.com/xtaci/kcp-go) - reliable UDP implementation
```