package util

import (
	"log"
)

// Bug writes a line to os.Stderr with prefix 'BUG' to emphasize an issue in the program.
func Bug(line string) {
	log.Println("BUG! " + line)
}

// Debug writes a line to os.Stderr with prefix 'debug'.
func Debug(line string) {
	log.Println("[debug] " + line)
}

// Info writes a line to os.Stderr with prefix 'info'.
func Info(line string) {
	log.Println("[info] " + line)
}

// Warn writes a line to os.Stderr with prefix 'warn'.
func Warn(line string) {
	log.Println("[warn] " + line)
}

// Error writes a line to os.Stderr with prefix 'ERROR'.
func Error(line string) {
	log.Println("ERROR: " + line)
}
