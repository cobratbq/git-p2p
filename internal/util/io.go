package util

import (
	"io"
	"strconv"
)

// CopyNoWarning copies contents from in to out. Do NOT log anything in case
// transfer interrupts.
func CopyNoWarning(out io.Writer, in io.Reader) {
	n, _ := io.Copy(out, in)
	Debug("git process transfer: done. (" + strconv.FormatInt(n, 10) + " bytes)")
}

// CopyWithWarning copies contents from in to out. It logs a warning in case
// transfer interrupts.
func CopyWithWarning(out io.Writer, in io.Reader) {
	var n int64
	var err error
	if n, err = io.Copy(out, in); err != nil {
		Warn("Failed to copy all content to connection: " + err.Error())
	}
	Debug("git process transfer: done. (" + strconv.FormatInt(n, 10) + " bytes)")
}
