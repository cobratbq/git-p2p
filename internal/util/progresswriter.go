package util

import (
	"fmt"
	"io"
	"time"
)

// ProgressWriter represents a writer that keeps progress information.
type ProgressWriter struct {
	Total uint64
	Cum   uint64
	done  chan uint
}

// NewProgressWriter constructs a new progress writer given predefined total.
func NewProgressWriter(total uint64) *ProgressWriter {
	return &ProgressWriter{
		Total: total,
		Cum:   0,
		done:  make(chan uint, 0),
	}
}

// Write processes a write-call and updates progress information accordingly.
func (p *ProgressWriter) Write(data []byte) (int, error) {
	n := len(data)
	p.Cum += uint64(n)
	return n, nil
}

// StartReporting starts a fiber that periodically reports on write progress.
func (p *ProgressWriter) StartReporting(dest io.Writer, period time.Duration) {
	previous := uint64(0)
	go func() {
		for {
			select {
			case <-p.done:
				return
			default:
				speed := (float32(p.Cum-previous) / (float32(period) / float32(time.Second))) / 1024
				progress := float32(p.Cum) / float32(p.Total)
				dest.Write([]byte(fmt.Sprintf("progress: %.1f%% (%.0f KiB/s)\n", progress*100, speed)))
				previous = p.Cum
			}
			time.Sleep(period)
		}
	}()
}

// Done signals progress writer is done. This cancels the reporting thread and
// assigns total number of bytes to cumulative field to ensure 100% is reached.
func (p *ProgressWriter) Done() {
	p.Cum = p.Total
	close(p.done)
}
