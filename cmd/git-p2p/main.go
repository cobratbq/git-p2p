package main

import (
	"os"
	"path/filepath"
	"strings"

	"dannyvanheumen.nl/p2p/internal/util"
	"dannyvanheumen.nl/p2p/svc/git"
	"github.com/cobratbq/goutils/std/builtin"
	os_ "github.com/cobratbq/goutils/std/os"
	gogit "gopkg.in/src-d/go-git.v4"
)

// git-p2p progress: [☐☑☒]
//  ☑ git-remote-p2p
//  ☑ expose repository through `git push p2p`
//  ☐ untangle mixup of local IPC and peer connections: git transport should go over peer connections
//  ☐ transform references to reference containing peer identity
//  ☐ complete p2p network connections
// extra:
//  ☐ list currently exposed repostories with repoID
//  ☐ expose specific references through `git push p2p`
func main() {
	if os.Args[0] == "git-remote-p2p" || strings.HasSuffix(os.Args[0], "/git-remote-p2p") {
		// divert calls from `git-remote-p2p` (accessed through symlink)
		if len(os.Args) != 3 || !git.FullRemoteURLPattern.MatchString(os.Args[2]) {
			os.Stderr.WriteString("Usage: git-remote-p2p <remote-name> <remote-url>\n")
			os.Exit(1)
		}
		os.Exit(handleGitRemoteP2P())
	}

	var command string
	if len(os.Args) < 2 {
		command = ""
	} else {
		command = os.Args[1]
	}
	switch command {
	case "setup":
		os.Exit(handleSetup())
	case "status":
		os.Exit(handleStatus())
	case "":
		showHelp()
		handleStatus()
		os.Exit(0)
	default:
		util.Error("unknown command: " + command + "\n")
		os.Exit(1)
	}
}

func showHelp() {
	os.Stderr.WriteString("Usage: git-p2p\n...\n")
	os.Stderr.WriteString("\ngit-p2p performs git operations over a local, ad-hoc set-up peer-to-peer network.\n")
}

func handleSetup() int {
	var repo *gogit.Repository
	if repo, _ = openRepository(os_.WorkingDir()); repo == nil {
		util.Error("Failed to open repository.")
		return 1
	}
	if err := git.SetUpRemote(repo); err != nil {
		if remoteExistsErr, ok := err.(*git.ErrRemoteExists); ok {
			os.Stderr.WriteString("A P2P remote is already configured:\n  Name: " +
				remoteExistsErr.Name + "\n  URL: " + remoteExistsErr.URL + "\n")
			os.Stderr.WriteString("\nRun 'git p2p status' for more information.\n")
		} else {
			os.Stderr.WriteString("Failed to set up P2P remote: " + err.Error() + "\n")
		}
		return 1
	}
	return 0
}

func handleStatus() int {
	var repo *gogit.Repository
	if repo, _ = openRepository(os_.WorkingDir()); repo == nil {
		util.Error("Failed to open repository.")
		return 1
	}
	status := git.CheckRemote(repo)
	if !status.RepositoryAccessible {
		os.Stderr.WriteString("Cannot find git repository.\n")
		return 1
	}

	if status.RemoteName == "" {
		os.Stderr.WriteString("No P2P configuration found.\n\nRun 'git p2p setup' to set up the default remote 'p2p' based on configuration from remote 'origin'.\n")
	} else {
		os.Stderr.WriteString("P2P remote configured:\n")
		os.Stderr.WriteString("  name: " + status.RemoteName + "\n")
		os.Stderr.WriteString("  url: " + status.RemoteURL + "\n")
		os.Stderr.WriteString("\nUse p2p with any of the commands:\n  git fetch " +
			status.RemoteName + "\n  git push " + status.RemoteName + "\n")
	}
	return 0
}

func openRepository(path string) (*gogit.Repository, string) {
	builtin.Require(filepath.IsAbs(path), "Absolute path required.")
	// TODO check that this works correctly on Windows systems.
	if path == "/" {
		return nil, ""
	}
	util.Debug("Opening '" + path + "' ...")
	if repo, err := gogit.PlainOpen(path); err == nil {
		return repo, path
	}
	return openRepository(filepath.Dir(path))
}
