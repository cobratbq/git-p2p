package main

import (
	"bufio"
	"bytes"
	"io"
	"os"
	"strconv"
	"strings"
	"sync"
	"time"

	"dannyvanheumen.nl/p2p/internal/util"
	"dannyvanheumen.nl/p2p/ipc"
	"dannyvanheumen.nl/p2p/svc/git"
	"github.com/cobratbq/goutils/std/builtin"
	io_ "github.com/cobratbq/goutils/std/io"
	os_ "github.com/cobratbq/goutils/std/os"
)

// handleGitRemoteP2P implements git's support for remote transport 'p2p':
// git-remote-p2p. This implements, basically, `git-remote-ext` without needing
// to perform a subsequent call to the process that tunnels the data into the
// transport. Instead the method call `connect` implements the 'p2p' transport.
func handleGitRemoteP2P() int {
	// TODO support whitespace in remote URL or alternative guard against it more thoroughly.
	builtin.Require(strings.IndexByte(os.Args[2], ' ') == -1,
		"we cannot currently handle unescaped whitespace in the remote URL.")
	// remoteName := os.Args[1]
	repoID := git.ExtractRepoID(os.Args[2])
	peerID := git.ExtractPeerID(os.Args[2])
	reader := bufio.NewReader(os.Stdin)
	for {
		command, err := reader.ReadBytes('\n')
		if err != nil {
			util.Error(err.Error())
			return 1
		}
		command = bytes.TrimRight(command, "\n")

		parts := strings.SplitN(string(command), " ", 2)
		switch parts[0] {
		case "capabilities":
			// offer `git` the ability to use this remote to connect to
			// somewhere given the remote location's name and URL.
			os.Stdout.WriteString("*connect\n\n")
		case "connect":
			// `git` issues the 'connect' command. Establish a connection with
			// the remote location specified in 'os.Args[2]'. As part of the
			// 'connect' command, an additional parameter (space-separated) is
			// provided that prescribes how to communicate, either
			// `git-fetch-pack` or `git-upload-pack`. This is the git plumbing
			// that refers to either gathering information for the ultimate
			// downloading of new work ("fetch") or the uploading of new work
			// ("push"). Once connected, pass-through everything from `git` to
			// connection and vice-versa.
			os.Stdout.WriteString("\n")
			connect(repoID, peerID, parts[1])
			return 0
		default:
			// no other commands are supported by this remote as everything
			// else relies on the connection being established.
			os.Stderr.WriteString("Bad command\n")
			return 1
		}
	}
}

// connect connects with the P2P agent, initiates contact with the git-p2p
// service, and then starts to transparently transfer data between
// os.Stdin/os.Stdout (git process) and the connection (P2P agent).
func connect(repoID, peerID, command string) {
	// FIXME re-evaluate if this is the right behavior, once git-p2p has matured.
	if command == "git-receive-pack" {
		// For now, let's not push immediately to the remote. It seems rude to
		// update their files without their knowledge.
		peerID = ""
	}
	conn, err := ipc.ConnectService("dannyvanheumen.nl:git=0", peerID)
	if err != nil {
		util.Error("Failed to connect to P2P service: " + err.Error())
		os.Exit(1)
	}
	defer io_.CloseLogged(conn, "failed to close connection: %+v")

	// FIXME what happens if we clone one repository inside another. It will probably discover the outer repository and send that over instead of correctly determining a close operation hence no directory. (Consider checking remote ID and URL for further confirmation.)
	var repoDir string
	if repo, path := openRepository(os_.WorkingDir()); repo != nil {
		repoDir = path
	} else {
		repoDir = "-"
	}

	// FIXME "git-upload-pack w/o repoDir" is only true if not nested ...
	// clone: git-upload-pack w/o repoDir
	// fetch: git-upload-pack w/ repoDir
	//  push: git-receive-pack w/ repoDir
	conn.Write([]byte(command + " " + repoID + " " + repoDir))

	timestamp := strconv.FormatInt(time.Now().UnixNano(), 16)
	var wg sync.WaitGroup
	wg.Add(1)
	go func(out io.Writer, in io.Reader) {
		defer wg.Done()
		file, err := os.Create("dump-output-" + timestamp)
		if err != nil {
			panic(err.Error())
		}
		defer io_.CloseLogged(file, "failed to close outgoing communication dump: %+v")
		out = io.MultiWriter(out, file)
		util.CopyNoWarning(out, in)
	}(os.Stdout, conn)

	file, err := os.Create("dump-input-" + timestamp)
	if err != nil {
		panic(err.Error())
	}
	defer io_.CloseLogged(file, "failed to close incoming communication dump: %+v")
	out := io.MultiWriter(conn, file)
	util.CopyWithWarning(out, os.Stdin)

	wg.Wait()
}
