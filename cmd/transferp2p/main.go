package main

import (
	"crypto/sha256"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"strconv"
	"time"

	"dannyvanheumen.nl/p2p/internal/util"
	"dannyvanheumen.nl/p2p/ipc"
	"dannyvanheumen.nl/p2p/protocol"
	"dannyvanheumen.nl/p2p/svc/filetransfer"
	"github.com/cobratbq/goutils/std/builtin"
	io_ "github.com/cobratbq/goutils/std/io"
	"github.com/xtaci/smux"
)

func main() {
	if len(os.Args) < 3 {
		// FIXME consider swapping 'file' and 'peer' such that we can send multiple files.
		os.Stderr.WriteString("Usage: transferp2p send <file> <peer>\n")
		os.Stderr.WriteString("Usage: transferp2p receive <directory>\n")
		return
	}
	switch os.Args[1] {
	case "send":
		send()
	case "receive":
		receive()
	}
}

const serviceID = "dannyvanheumen.nl:filetransfer=0"

func send() {
	fileName := os.Args[2]
	var peerID string
	if len(os.Args) > 3 {
		peerID = os.Args[3]
	} else {
		peerID = ""
	}
	conn, err := ipc.ConnectService(serviceID, peerID)
	if err == os.ErrNotExist {
		util.Error("Peer '" + peerID + "' does not exist.")
		return
	}
	builtin.RequireSuccess(err, "failed to acquire access to filetransfer service: %+v")
	defer io_.CloseLogged(conn, "failed to close connection: %+v")
	input, err := os.Open(fileName)
	if err != nil {
		os.Stderr.WriteString("Failed to open '" + fileName + "': " + err.Error())
		return
	}
	defer io_.CloseLogged(input, "failed to close input file: %+v")
	info, err := input.Stat()
	builtin.RequireSuccess(err, "failed to acquire metadata for "+fileName)
	// TODO: detect content-type using library? (now assumes 'application/octet-stream')
	if err := protocol.WriteCommand(conn, []byte("offer=0"), []byte("name="+fileName),
		[]byte("size="+strconv.FormatInt(info.Size(), 10)),
		[]byte("type=application/octet-stream"), []byte{}); err != nil {

		os.Stderr.WriteString("Failed to write commands: " + err.Error())
		return
	}
	response, err := protocol.ReadCommand(conn)
	if err != nil {
		util.Error("transfer aborted: " + err.Error())
	}
	progress := util.NewProgressWriter(uint64(info.Size()))
	progress.StartReporting(os.Stderr, 1*time.Second)
	switch string(response) {
	case protocol.ResponseOk:
		checksum := sha256.New()
		io.Copy(io.MultiWriter(conn, checksum, progress), input)
		defer progress.Done()
		conn.Write(checksum.Sum(nil))
	case protocol.ResponseFail:
		util.Info("aborting due to receiver-side failure.")
	default:
		builtin.Unsupported("received unsupported command: " + string(response))
	}
}

func receive() {
	conn, err := ipc.ConnectService(serviceID, "")
	builtin.RequireSuccess(err, "failed to acquire access to filetransfer service: %+v")
	defer io_.CloseLogged(conn, "failed to close connection: %+v")
	err = protocol.WriteCommand(conn, []byte("receive=0"))
	builtin.RequireSuccess(err, "failed to write receive-command to file transfer service: %+v")
	session, err := smux.Server(conn, smux.DefaultConfig())
	builtin.RequireSuccess(err, "failed to establishing connection multiplexing: %+v")
	util.Info("Waiting for file transfer offer ...")
	for {
		s, err := session.AcceptStream()
		if err == io.EOF {
			util.Info("Lost connection to server.")
			break
		}
		if err != nil {
			util.Warn("Error accepting stream: " + err.Error())
			continue
		}
		defer io_.CloseLogged(s, "failure closing receiver stream after illegal command: %+v")
		cmd, err := protocol.ReadCommand(s)
		if err != nil {
			io_.CloseLogged(s, "closing for protocol violation: %+v")
			continue
		}
		switch string(cmd) {
		case "offer=0":
			handleOffer(s)
		default:
			protocol.WriteCommand(s, []byte(protocol.ResponseBad))
			util.Debug("transferp2p: received illegal command: " + string(cmd))
		}
	}
}

func handleOffer(conn io.ReadWriteCloser) {
	defer io_.CloseLogged(conn, "failed to close incoming file transfer stream: %+v")
	props, err := filetransfer.ReadProperties(conn)
	if err != nil || !props.Valid() {
		protocol.WriteCommand(conn, []byte(protocol.ResponseBad))
		return
	}
	// FIXME strip/sanitize file name to prevent path traversal and other weird shit.
	dest, err := os.Create(filepath.Base(props.Name))
	if err != nil {
		// FIXME should indicate illegal state, not bad request.
		protocol.WriteCommand(conn, []byte(protocol.ResponseBad))
		return
	}
	protocol.WriteCommand(conn, []byte(protocol.ResponseOk))
	util.Info("Writing file: " + props.Name + ", " + strconv.FormatInt(props.Size, 10) + " bytes")
	start := time.Now().Unix()
	progress := util.NewProgressWriter(uint64(props.Size))
	progress.StartReporting(os.Stderr, 2*time.Second)
	err = filetransfer.Transfer(dest, conn, progress, props.Size)
	defer progress.Done()
	if err != nil {
		util.Error("File transfer unsuccessful: " + err.Error())
	} else {
		duration := time.Now().Unix() - start
		util.Info(fmt.Sprintf("Transfer finished successfully. (%d seconds, %d KiB/s)", duration, props.Size/duration/1024))
	}
}
