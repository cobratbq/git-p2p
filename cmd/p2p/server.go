package main

import (
	"bytes"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"io"
	"net"
	"os"
	"strconv"
	"strings"
	"sync"

	"dannyvanheumen.nl/p2p/internal/util"
	"dannyvanheumen.nl/p2p/ipc"
	"dannyvanheumen.nl/p2p/net/udp"
	"dannyvanheumen.nl/p2p/protocol"
	"dannyvanheumen.nl/p2p/svc/filetransfer"
	"dannyvanheumen.nl/p2p/svc/git"
	"github.com/cobratbq/goutils/std/builtin"
	cryptorand "github.com/cobratbq/goutils/std/crypto/rand"
	io_ "github.com/cobratbq/goutils/std/io"
	"github.com/cobratbq/goutils/std/math/rand"
	"github.com/xtaci/smux"
)

const (
	// reserve '0' for possible later UNKNOWN/UNDEFINED purpose
	_ byte = iota

	// typeIPCConnection indicates (trusted) IPC connection on local machine
	typeIPCConnection

	// typePeerConnection indicates (untrusted) peer connection (over network)
	typePeerConnection
)

func executeServer() {
	var id protocol.PeerID
	cryptorand.MustReadBytes(id[:])
	util.Debug("Identifier: " + hex.EncodeToString(id[:]))

	var h = handler{
		ID: id,
		Services: map[string]P2PService{
			"dannyvanheumen.nl:git=0":          git.NewService(),
			"dannyvanheumen.nl:filetransfer=0": filetransfer.NewService(),
		},
		Peers: make(map[protocol.PeerID]*record, 0),
	}

	var wg sync.WaitGroup
	wg.Add(2)
	go setUpIPC(&h, &wg)
	go setUpPeerListener(&h, &wg)
	util.Info("P2P agent started.")
	wg.Wait()
}

func setUpIPC(h *handler, wg *sync.WaitGroup) {
	defer wg.Done()
	server, err := ipc.Establish()
	builtin.RequireSuccess(err, "failed to establish IPC socket")
	defer io_.CloseLogged(server, "failed to close server connection: %+v")
	util.Info("Listening for IPC on address: " + server.Addr().String())
	h.IPCCommandIndex = map[string]func(io.ReadWriter){
		protocol.CmdConnect: h.Connect,
		protocol.CmdMux:     h.Mux,
		protocol.CmdStatus:  h.Status,
		protocol.CmdService: h.ServicePassthroughIPC,
	}
	for {
		conn, err := server.Accept()
		if err != nil {
			util.Warn("Failed to accept IPC connection: " + err.Error())
			continue
		}

		// Upon incoming connection, write announcement with agent capabilities.
		// Don't expect announcement from other party yet, as they have contacted
		// us and may only be single request.
		err = protocol.WriteAnnouncement(conn, h.ID[:], builtin.ExtractStringKeys(h.IPCCommandIndex)...)
		builtin.RequireSuccess(err, "failed to send announcement: %+v")

		go h.HandleIPC(conn)
	}
}

// TODO: consider having the server dump a list of available top-level commands upon establishing connection.
func setUpPeerListener(h *handler, wg *sync.WaitGroup) {
	defer wg.Done()

	conn, err := net.ListenUDP("udp", &net.UDPAddr{IP: net.IPv4zero, Port: 9999, Zone: ""})
	builtin.RequireSuccess(err, "Failed to establishing server for peer connections: %s")
	defer io_.CloseLogged(conn, "Failed to close UDP connection: %s")

	server, err := udp.Listen(conn)
	builtin.RequireSuccess(err, "failed to set up peer address for networking")
	defer io_.CloseLogged(server, "failed to close peer server: %+v")
	util.Info("Listening for peers on address: " + server.Addr().String())
	h.listener = server

	h.PeerCommandIndex = map[string]func(io.ReadWriter){
		protocol.CmdRegister: h.Register,
		protocol.CmdService:  h.ServicePassthroughPeer,
	}
	for {
		conn, err := server.Accept()
		if err != nil {
			util.Warn("Failed to accept peer connection: " + err.Error())
			// FIXME change as necessary
			panic("Aborting altogether for debugging purposes.")
		}

		// Receive "hey, I exist!" message to trigger announcement and peer registration.
		// FIXME currently we need to receive some data from other peer, as UDP is connectionless and we wouldn't know of their existance otherwise.
		var version [2]byte
		conn.Read(version[:])

		// Upon incoming connection, write announcement with agent capabilities.
		// Don't expect announcement from other party yet, as they have contacted
		// us and may only be single request.
		err = protocol.WriteAnnouncement(conn, h.ID[:], builtin.ExtractStringKeys(h.PeerCommandIndex)...)
		builtin.RequireSuccess(err, "failed to send announcement: %+v")

		go h.HandlePeer(conn)
	}
}

// FIXME go through handler implementations and replace panics with ResponseBad for all applicable cases.
type handler struct {
	listener *udp.Listener

	ID       protocol.PeerID
	Services map[string]P2PService
	Peers    peersindex

	PeerCommandIndex map[string]func(io.ReadWriter)
	IPCCommandIndex  map[string]func(io.ReadWriter)
}

func (h *handler) HandlePeer(conn io.ReadWriteCloser) {
	defer io_.CloseLogged(conn, "failed to close peer connection: %+v")
	defer func() {
		if v := recover(); v != nil {
			util.Warn(fmt.Sprintf("Peer connection handling panicked: %+v", v))
			protocol.WriteCommand(conn, []byte(protocol.ResponseFail))
		}
	}()

	command, err := protocol.ReadCommand(conn)
	if err != nil {
		util.Warn("aborted: (Peer) client-side protocol violation: " + err.Error())
		return
	}

	if operation, ok := h.PeerCommandIndex[string(command)]; ok {
		operation(conn)
	} else {
		protocol.WriteCommand(conn, []byte(protocol.ResponseBad))
		util.Debug("aborted: (Peer) received unknown command: " + string(command))
	}
}

func (h *handler) HandleIPC(conn io.ReadWriteCloser) {
	defer io_.CloseLogged(conn, "failed to close local IPC connection: %+v")
	defer func() {
		if v := recover(); v != nil {
			util.Warn("IPC connection handling panicked: %+v")
			protocol.WriteCommand(conn, []byte(protocol.ResponseFail))
		}
	}()

	command, err := protocol.ReadCommand(conn)
	if err != nil {
		util.Warn("aborted: (IPC) client-side protocol violation: " + err.Error())
		return
	}

	if operation, ok := h.IPCCommandIndex[string(command)]; ok {
		operation(conn)
	} else {
		protocol.WriteCommand(conn, []byte(protocol.ResponseBad))
		util.Debug("aborted: (IPC) received unknown command: " + string(command))
	}
}

// Register registers a (remote) peer. After successful registration, we
// immediately connect the multiplexer, as the peer connection needs to be
// available for multiple uses.
func (h *handler) Register(conn io.ReadWriter) {
	util.Debug("Handling register request for incoming peer ...")
	ann, err := protocol.ReadAnnouncement(conn)
	builtin.RequireSuccess(err, "failed to read announcement from registering peer: %+v")

	v := rand.Uint64NonZero()
	peer, err := h.Peers.Take(ann.ID, v)
	if err == os.ErrExist {
		protocol.WriteCommand(conn, []byte(protocol.ResponseFail))
		util.Info("Registration failed because the peer connection for this peer ID is already managed.")
		return
	}
	builtin.RequireSuccess(err, "unexpected failure taking ownership of peer record: %+v")
	defer func(r *record) {
		r.Conn = nil
		h.Peers.Release(ann.ID, v)
	}(peer)
	peer.Commands = ann.Commands

	builtin.RequireSuccess(protocol.WriteCommand(conn, []byte(protocol.ResponseOk)),
		"failed to send success confirmation")

	mux, err := smux.Server(&io_.NopCloser{Rw: conn}, smux.DefaultConfig())
	builtin.RequireSuccess(err, "failed to establish connection multiplexing")
	// FIXME how to store peer.Addr if it isn't available at this point?
	peer.Conn = mux

	util.Info("Successfully registered peer '" + hex.EncodeToString(ann.ID[:]) + "'.")

	for {
		stream, err := mux.AcceptStream()
		if err == io.ErrClosedPipe {
			break
		}
		builtin.RequireSuccess(err, "unexpected error while accepting stream: %+v")
		go h.HandlePeer(stream)
	}
	util.Info(fmt.Sprintf("connection to %0x was lost.", ann.ID[:]))
	// TODO attempt to reestablish connection with address information available in the peer record?
}

func (h *handler) Connect(conn io.ReadWriter) {
	util.Debug("Handling 'connect' request to other peer ...")
	result := protocol.ResponseFail
	// FIXME read identity and other parameters. (don't abuse ReadCommand for this) (define ReadParameter?)
	remote := protocol.Expect(protocol.ReadCommand(conn))
	parts := bytes.SplitN(remote, []byte{'='}, 2)
	defer func(r *string) {
		protocol.WriteCommand(conn, []byte(*r))
	}(&result)

	p2pconn, err := h.listener.Dial(string(parts[1]))
	if err != nil {
		util.Warn("failed to connect to " + string(parts[1]) + ": " + err.Error())
		return
	}

	done := make(chan struct{}, 1)

	go func(done chan<- struct{}) {
		defer func() {
			close(done)
			io_.CloseLogged(p2pconn, "failed to close peer connection: %+v")
		}()

		// FIXME nudge! currently we need to send a package to announce our existance (currently we use protocol version)
		p2pconn.Write([]byte{0, 0})

		util.Debug("awaiting peer announcement ...")
		ann, err := protocol.ReadAnnouncement(p2pconn)
		builtin.RequireSuccess(err, "failed to read announcement from other peer: %s")

		util.Info("Established connection with peer " + p2pconn.RemoteAddr().String())
		err = protocol.WriteCommand(p2pconn, []byte("register=0"))
		builtin.RequireSuccess(err, "unexpected failure writing to newly established P2P-connection")
		err = protocol.WriteAnnouncement(p2pconn, h.ID[:], builtin.ExtractStringKeys(h.PeerCommandIndex)...)
		builtin.RequireSuccess(err, "failed to send announcement for registration: %+v")

		// FIXME keep here or move after peer management is done?
		confirm, err := protocol.ReadCommand(p2pconn)
		builtin.RequireSuccess(err, "failed to receive confirmation of registration: %+v")
		if string(confirm) != protocol.ResponseOk {
			protocol.WriteCommand(conn, []byte(protocol.ResponseFail))
			return
		}

		v := rand.Uint64NonZero()
		peer, err := h.Peers.Take(ann.ID, v)
		if err == os.ErrExist {
			protocol.WriteCommand(conn, []byte(protocol.ResponseFail))
			util.Info("Registration failed because the peer connection is already managed.")
			return
		}
		if err != nil {
			protocol.WriteCommand(conn, []byte(protocol.ResponseFail))
			util.Info("Registration failed for unknown reason: " + err.Error())
			return
		}
		defer func(r *record) {
			r.Conn = nil
			h.Peers.Release(ann.ID, v)
		}(peer)
		peer.Commands = ann.Commands

		// multiplex peer connection such that it can be reused and parallelized indefinitely
		mux, err := smux.Client(p2pconn, smux.DefaultConfig())
		builtin.RequireSuccess(err, "unexpected failure establishing connection multiplexing")
		peer.Addr = p2pconn.RemoteAddr().String()
		peer.Conn = mux
		util.Debug("Established connection with peer '" + peer.Addr + "'.")

		// initialization finished successfully
		done <- struct{}{}

		// establish new fiber that listens to requests from newly joined peer
		for {
			stream, err := mux.AcceptStream()
			if err == io.ErrClosedPipe {
				break
			}
			builtin.RequireSuccess(err, "failure in accepting connection stream")
			go h.HandlePeer(stream)
		}
	}(done)

	if _, ok := <-done; ok {
		result = protocol.ResponseOk
	}
}

// Mux stacks multiplexing on top of an existing connection. We assume that the
// connection is established for a single operation. By making 'multiplexing'
// the single operation, we introduce reuse. mux is blocking and new streams
// are handled concurrently.
func (h *handler) Mux(conn io.ReadWriter) {
	util.Debug("Handling connection multiplexing request ...")
	mux, err := smux.Server(&io_.NopCloser{Rw: conn}, smux.DefaultConfig())
	builtin.RequireSuccess(err, "failed to multiplex IPC connection")
	defer io_.CloseLogged(mux, "failed to close multiplexed IPC connection: %+v")
	for {
		stream, err := mux.AcceptStream()
		if err == io.ErrClosedPipe {
			break
		}
		builtin.RequireSuccess(err, "unexpected error while accepting stream")
		go h.HandleIPC(stream)
	}
}

func (h *handler) Status(conn io.ReadWriter) {
	status := status{
		ID:       hex.EncodeToString(h.ID[:]),
		PID:      os.Getpid(),
		Peers:    make(map[string][]string, len(h.Peers)),
		Services: make([]string, len(h.Services)),
	}
	for ID, peer := range h.Peers {
		status.Peers[hex.EncodeToString(ID[:])] = builtin.ExtractStringKeys(peer.Commands)
	}
	status.Services = builtin.ExtractStringKeys(h.Services)
	data, err := json.Marshal(&status)
	builtin.RequireSuccess(err, "failed to serialize agent status: %+v")
	_, err = conn.Write(data)
	builtin.RequireSuccess(err, "failed to write status to connection: %+v")
}

type status struct {
	ID       string              `json:"id"`
	PID      int                 `json:"pid"`
	Peers    map[string][]string `json:"peers"`
	Services []string            `json:"services"`
}

func (h *handler) ServicePassthroughPeer(conn io.ReadWriter) {
	service, err := protocol.ReadCommand(conn)
	if err != nil || !ipc.ServiceIDFormat.Match(service) {
		protocol.WriteCommand(conn, []byte(protocol.ResponseBad))
		return
	}
	h.servicePassthrough(typePeerConnection, conn, string(service))
}

// FIXME consider defining 'bad' response with body to explain what is invalid.
func (h *handler) ServicePassthroughIPC(conn io.ReadWriter) {
	serviceBytes, err := protocol.ReadCommand(conn)
	if err != nil || !ipc.ServiceIDFormat.Match(serviceBytes) {
		protocol.WriteCommand(conn, []byte(protocol.ResponseBad))
		return
	}
	service := string(serviceBytes)
	// FIXME use other function than ReadCommand
	peerIDHex, err := protocol.ReadCommand(conn)
	if err != nil || !strings.HasPrefix(string(peerIDHex), "remote=") {
		protocol.WriteCommand(conn, []byte(protocol.ResponseBad))
		return
	}
	var peer protocol.PeerID
	if peer, err = protocol.ParsePeerID(peerIDHex[7:], h.ID); err != nil {
		protocol.WriteCommand(conn, []byte(protocol.ResponseBad))
		return
	}

	if peer == h.ID {
		// IPC requests local service pass-through
		h.servicePassthrough(typeIPCConnection, conn, service)
	} else if record, ok := h.Peers[peer]; ok && record.Conn != nil {
		// IPC requests remote service pass-through
		remote, err := record.Conn.OpenStream()
		builtin.RequireSuccess(err, "failed to open communication channel to peer: %+v")
		protocol.WriteCommand(remote, []byte(protocol.CmdService), serviceBytes)
		response, err := protocol.ReadCommand(remote)
		builtin.RequireSuccess(err, "failed to receive confirmation of service pass-through: %+v")
		switch string(response) {
		case protocol.ResponseOk:
			protocol.WriteCommand(conn, []byte(protocol.ResponseOk))
			// establish communication pass-through from local agent to remote peer
			go util.CopyWithWarning(remote, conn)
			util.CopyWithWarning(conn, remote)
		case protocol.ResponseBad:
			protocol.WriteCommand(conn, []byte(protocol.ResponseBad))
		case protocol.ResponseFail:
			protocol.WriteCommand(conn, []byte(protocol.ResponseFail))
		}
	} else {
		// IPC requests for unknown or unavailable peer.
		protocol.WriteCommand(conn, []byte(protocol.ResponseBad))
		return
	}
}

// TODO define, reserve groupID-less "commands" for service-passthrough-interaction, such as service=1\n:list for listing available services?
func (h *handler) servicePassthrough(connType byte, conn io.ReadWriter, service string) {
	if svc, ok := h.Services[string(service)]; ok {
		i := strings.IndexByte(service, '=')
		version, err := strconv.ParseUint(string(service[i+1:]), 10, 64)
		builtin.RequireSuccess(err, "service identifier is illegal: "+string(service))
		defer builtin.RecoverLoggedStackTrace("service panicked: %+v")
		protocol.WriteCommand(conn, []byte(protocol.ResponseOk))
		switch connType {
		case typeIPCConnection:
			svc.HandleIPC(conn, version)
		case typePeerConnection:
			svc.HandlePeer(conn, version)
		default:
			builtin.Unsupported(fmt.Sprintf("BUG: unsupported connection type: %0x", connType))
		}
	} else {
		protocol.WriteCommand(conn, []byte(protocol.ResponseBad))
	}
}

// P2PService is the interface definitions for services that hook into the P2P
// transport. These services will receive the connection and will acquire full
// pass-through of all data from the point of hand-over on.
//
// The service description does not prescribe any format for the service. At
// the moment of hand-over, any format - binary or text-based - is allowed.
// FIXME consider moving this to 'service.go' in 'svc' package, or something similar.
// FIXME reduce amount of confirmations (ok=1) by broadcasting valid commands directly after connecting/service pass-through.
type P2PService interface {

	// HandleIPC handles the local, trusted IPC connection as it is handed over
	// to the service.
	HandleIPC(io.ReadWriter, uint64)

	// HandlePeer handles the untrusted peer's connection as it is handed over
	// to the service.
	HandlePeer(io.ReadWriter, uint64)
}
