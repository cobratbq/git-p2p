package main

import (
	"io"
	"os"

	"dannyvanheumen.nl/p2p/internal/util"
	"dannyvanheumen.nl/p2p/ipc"
	"dannyvanheumen.nl/p2p/protocol"
	io_ "github.com/cobratbq/goutils/std/io"
)

func executeStatus() {
	// connect to local p2p agent
	conn, _, err := ipc.Connect()
	if err != nil {
		util.Error("Failed to connect to p2p agent: " + err.Error())
		os.Exit(1)
	}
	defer io_.CloseLogged(conn, "failed to close connection: %+v")
	protocol.WriteCommand(conn, []byte(protocol.CmdStatus))
	io.Copy(os.Stdout, conn)
	os.Stdout.Write([]byte{'\n'})
}
