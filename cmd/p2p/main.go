package main

import (
	"os"
	"strings"

	"dannyvanheumen.nl/p2p/internal/util"
)

// FIXME professionalize response code handling, i.e. don't compare to "ok=1".
// FIXME implement 'fail=0' response as recovering from panic.
// FIXME mass-replace responses (bad=0, ok=1) with constants.
// FIXME broadly apply coding conventions in existing code base.
// FIXME reduce amount of confirmations (ok=1) by broadcasting valid commands directly after connecting/service pass-through.
// FIXME check for command availability before issuing (use announcement)
func main() {
	if len(os.Args) < 2 {
		os.Stderr.WriteString("Usage: p2p <command>\n\nCommands:\n connect\n server\n status\n")
		// FIXME display help information and list of subcommands
		return
	}
	util.Debug(strings.Join(os.Args, " "))
	switch os.Args[1] {
	case "server":
		executeServer()
	case "status":
		executeStatus()
	case "connect":
		executeConnect()
	default:
		os.Stderr.WriteString("Bad command.\n")
		os.Exit(1)
	}
}
