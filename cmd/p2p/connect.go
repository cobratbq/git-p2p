package main

import (
	"io"
	"os"

	"dannyvanheumen.nl/p2p/internal/util"
	"dannyvanheumen.nl/p2p/ipc"
	"dannyvanheumen.nl/p2p/protocol"
	"github.com/cobratbq/goutils/std/builtin"
)

func executeConnect() {
	conn, _, err := ipc.Connect()
	builtin.RequireSuccess(err, "failed to establish connection: %s")
	builtin.RequireSuccess(protocol.WriteCommand(conn, []byte(protocol.CmdConnect), []byte("remote="+os.Args[2])),
		"failed to send command for establishing a new connection")
	result, err := protocol.ReadCommand(conn)
	if err == io.ErrUnexpectedEOF {
		util.Error("connection lost: " + err.Error())
		os.Exit(1)
	}
	builtin.RequireSuccess(err, "failed to receive execution result: %s")
	// FIXME process result and exit with appropriate exit code.
	if string(result) != protocol.ResponseOk {
		util.Debug("Result: " + string(result))
		os.Exit(1)
	}
}
