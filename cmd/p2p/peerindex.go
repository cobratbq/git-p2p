package main

import (
	"os"
	"sync/atomic"

	"dannyvanheumen.nl/p2p/protocol"
	"github.com/xtaci/smux"
)

type peersindex map[protocol.PeerID]*record

func (p peersindex) Take(id protocol.PeerID, ticket uint64) (*record, error) {
	var peer *record
	if peer = p[id]; peer == nil {
		peer = &record{}
		p[id] = peer
	}
	if !atomic.CompareAndSwapUint64(&peer.owned, 0, ticket) {
		return nil, os.ErrExist
	}
	return peer, nil
}

func (p peersindex) Release(id protocol.PeerID, ticket uint64) error {
	peer, ok := p[id]
	if !ok {
		panic("BUG: peer record has disappeared from the index.")
	}
	if !atomic.CompareAndSwapUint64(&peer.owned, ticket, 0) {
		return os.ErrInvalid
	}
	return nil
}

type record struct {
	owned    uint64
	Addr     string
	Conn     *smux.Session
	Commands map[string]struct{}
}
