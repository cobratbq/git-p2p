package filetransfer

import (
	"bytes"
	"io"
	"os"
	"strconv"

	"dannyvanheumen.nl/p2p/protocol"
	"github.com/cobratbq/goutils/std/builtin"
)

// Properties represents the properties in use in the file transfer process.
type Properties struct {
	Name string
	Size int64
	Type string
}

// Valid checks whether or not the properties are consistent, valid.
func (p *Properties) Valid() bool {
	return len(p.Name) > 0 && p.Size > 0 && len(p.Type) > 0
}

// ReadProperties reads the expected properties from the communication stream.
// FIXME: reconsider maximum size of property-line.
func ReadProperties(conn io.ReadWriter) (*Properties, error) {
	var properties Properties
	var line [1024]byte
	for {
		n, err := protocol.ReadLine(line[:], conn)
		if err != nil {
			return nil, err
		}
		if len(line[:n]) == 0 {
			if properties.Valid() {
				return &properties, nil
			}
			return nil, os.ErrInvalid
		}
		parts := bytes.SplitN(line[:n], []byte{'='}, 2)
		switch string(parts[0]) {
		case "size":
			properties.Size, err = strconv.ParseInt(string(parts[1]), 10, 64)
			if err != nil {
				return nil, os.ErrInvalid
			}
		case "name":
			properties.Name = string(parts[1])
		case "type":
			properties.Type = string(parts[1])
		default:
			builtin.Unsupported("unsupported property encountered: " + string(parts[1]))
		}
	}
}
