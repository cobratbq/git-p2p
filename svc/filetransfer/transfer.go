package filetransfer

import (
	"bytes"
	"crypto/sha256"
	"fmt"
	"io"
	"os"
)

// Transfer performs the actual transferring of bytes.
// FIXME do more error handling such that we report *anything* going wrong in the file transfer.
func Transfer(dest io.Writer, conn io.Reader, progress io.Writer, size int64) error {
	checksum := sha256.New()
	io.CopyN(io.MultiWriter(dest, checksum, progress), conn, size)
	var expected [32]byte
	if n, err := conn.Read(expected[:]); err != nil {
		return fmt.Errorf("failed to read expected checksum: %v", err)
	} else if n != len(expected) {
		return fmt.Errorf("read less than expected number of bytes for checksum: %v", os.ErrInvalid)
	}
	if !bytes.Equal(expected[:], checksum.Sum(nil)) {
		return fmt.Errorf("Invalid checksum: %v", os.ErrInvalid)
	}
	return nil
}
