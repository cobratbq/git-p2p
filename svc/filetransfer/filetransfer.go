package filetransfer

import (
	"io"
	"log"
	"os"
	"strconv"
	"sync/atomic"

	"dannyvanheumen.nl/p2p/internal/util"
	"dannyvanheumen.nl/p2p/protocol"
	"github.com/cobratbq/goutils/std/builtin"
	io_ "github.com/cobratbq/goutils/std/io"
	"github.com/cobratbq/goutils/std/math/rand"
	"github.com/xtaci/smux"
)

// Service is the main file transfer service type. It functions as the
// entrypoint into the file transfer service.
type Service struct {
	sync  uint64
	agent *agent
}

// NewService construts a new file transfer service.
func NewService() *Service {
	return &Service{
		sync:  0,
		agent: nil,
	}
}

// HandleIPC handles requests coming from local clients through IPC socket.
func (s *Service) HandleIPC(conn io.ReadWriter, version uint64) {
	cmdBytes, err := protocol.ReadCommand(conn)
	if err != nil {
		util.Warn("failed to read transfer command: " + err.Error())
		return
	}
	cmd := string(cmdBytes)
	switch cmd {
	case "receive=0":
		v := rand.Uint64NonZero()
		if !atomic.CompareAndSwapUint64(&s.sync, 0, v) {
			// FIXME to be implemented
			panic("To be implemented: bad shit happened")
		}
		defer func() {
			if !atomic.CompareAndSwapUint64(&s.sync, v, 0) {
				// FIXME to be implemented
				panic("To be implemented: bad shit happened")
			}
		}()
		mux, err := smux.Client(&io_.NopCloser{Rw: conn}, smux.DefaultConfig())
		builtin.RequireSuccess(err, "failed to multiplex connection: %+v")
		s.agent = newAgent(mux)
		for {
			s, err := mux.AcceptStream()
			if err == io.EOF {
				break
			}
			log.Printf("Err: %+v", err)
			io_.CloseLogged(s, "failed to close stream: %+v")
		}
	default:
		s.handle(conn, version, cmd)
	}
}

// HandlePeer handles requests from other peers through network connections.
func (s *Service) HandlePeer(conn io.ReadWriter, version uint64) {
	cmdBytes, err := protocol.ReadCommand(conn)
	if err != nil {
		util.Warn("failed to read transfer command: " + err.Error())
		return
	}
	s.handle(conn, version, string(cmdBytes))
}

func (s *Service) handle(conn io.ReadWriter, version uint64, cmd string) {
	switch cmd {
	case "offer=0":
		// FIXME: read additional metadata in lines following .. define suitable format. (Adopt HTTP metadata?)
		props, err := ReadProperties(conn)
		if err != nil {
			util.Warn("failed to read properties: " + err.Error())
			return
		}
		agent := s.agent
		if agent == nil {
			protocol.WriteCommand(conn, []byte(protocol.ResponseFail))
			util.Warn("File transfer aborted as there is no client to receive the file.")
			return
		}
		destination, err := agent.Persist(props)
		if err != nil {
			// FIXME handle error case
			builtin.Unsupported("To be implemented")
		}
		defer io_.CloseLogged(destination, "failed to close transfer destination: %+v")
		protocol.WriteCommand(conn, []byte(protocol.ResponseOk))
		// FIXME needs more error handling?
		util.CopyWithWarning(destination, conn)
	default:
		builtin.Unsupported("Unsupported command: " + string(cmd))
	}
}

type agent struct {
	session *smux.Session
}

func newAgent(session *smux.Session) *agent {
	return &agent{session: session}
}

func (a *agent) Persist(props *Properties) (io.WriteCloser, error) {
	s, err := a.session.OpenStream()
	builtin.RequireSuccess(err, "failed to open stream: %+v")
	// FIXME Don't use WriteCommand for sending properties.
	if err := protocol.WriteCommand(s, []byte("offer=0"), []byte("name="+props.Name),
		[]byte("size="+strconv.FormatInt(props.Size, 10)),
		[]byte("type="+props.Type), []byte{}); err != nil {

		util.Warn("Failed to write commands: " + err.Error())
		io_.CloseLogged(s, "failed to offer file transfer to receiving end: %+v")
		return nil, io.ErrShortWrite
	}
	confirm, err := protocol.ReadCommand(s)
	if err != nil {
		io_.CloseLogged(s, "failed to close stream connection to agent: %+v")
		return nil, os.ErrInvalid
	}
	// FIXME fine-tune OK handling
	if string(confirm) != protocol.ResponseOk {
		io_.CloseLogged(s, "failed to close stream connection to agent: %+v")
		return nil, io.ErrShortWrite
	}
	return s, nil
}
