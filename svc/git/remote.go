package git

import (
	"errors"
	"fmt"
	"regexp"

	"gopkg.in/src-d/go-git.v4"
	"gopkg.in/src-d/go-git.v4/config"
)

// TODO further separate p2p from git-p2p code.

// DefaultRemote is the name of the initial remote set upon cloning a repository.
const DefaultRemote = "origin"

// DefaultP2PRemote is the default name of the remote for use with p2p.
const DefaultP2PRemote = "p2p"

// P2PProtocolPrefix is the protocol part of the URL in case of p2p.
const P2PProtocolPrefix = "p2p://"

// FullRemoteURLPattern represents the p2p-protocol remote URL pattern.
// FIXME verify if pattern still matches with definitive peer identifier format
var FullRemoteURLPattern = regexp.MustCompile(`^p2p://(?:([0-9a-fA-F]+)@)?(.*)$`)

// ErrInvalidRepositoryLocation indicates that the specified directory location is not a repository.
var ErrInvalidRepositoryLocation = errors.New("invalid directory location for repository")

// RemoteStatus indicates information on the current status of the P2P remote configuration.
type RemoteStatus struct {
	// FIXME remove this property, is no longer useful
	RepositoryAccessible bool
	RemoteName           string
	RemoteURL            string
}

// ExtractRepoID extracts the repository ID from the "p2p://..." remote URL.
func ExtractRepoID(remoteURL string) string {
	matches := FullRemoteURLPattern.FindStringSubmatch(remoteURL)
	if matches == nil {
		panic("Failed to extract repo ID from remote URL " + remoteURL)
	}
	return matches[2]
}

// ExtractPeerID extracts the optional Peer identifier if used in the remote URL format.
func ExtractPeerID(remoteURL string) string {
	matches := FullRemoteURLPattern.FindStringSubmatch(remoteURL)
	if matches == nil {
		return ""
	}
	return matches[1]
}

// CheckRemote checks a given repository for its 'p2p' remote configuration,
// such that we can verify to what extent the p2p remote is configured
// correctly.
//
// Given this information it becomes possible to give detailed advice to the
// user on how to proceed to set up `git-p2p`, or alternative that everything
// is alright and how to proceed next to use `git-p2p`.
func CheckRemote(repo *git.Repository) RemoteStatus {
	var status = RemoteStatus{RepositoryAccessible: true}
	// FIXME search all remotes, instead of only 'origin', 'p2p'
	for _, remoteName := range []string{DefaultRemote, DefaultP2PRemote} {
		remote, err := repo.Remote(remoteName)
		if err == git.ErrRemoteNotFound {
			continue
		} else if err != nil {
			panic("Unexpected error: " + err.Error())
		} else if !FullRemoteURLPattern.MatchString(remote.Config().URLs[0]) {
			continue
		}
		status.RemoteName = remote.Config().Name
		status.RemoteURL = remote.Config().URLs[0]
		break
	}
	return status
}

// ErrRemoteExists indicates an error due to a remote configuration already existing.
type ErrRemoteExists struct {
	Name string
	URL  string
}

func (e *ErrRemoteExists) Error() string {
	return "Remote exists: " + e.Name + " " + e.URL
}

// SetUpRemote configures a p2p://-based remote called 'p2p' if it does not
// exist, and verifies the remote's URLs if it does exist.
// If the set up returns no errors, then the remote configuration is good to
// go.
func SetUpRemote(repo *git.Repository) error {
	status := CheckRemote(repo)
	if status.RepositoryAccessible && status.RemoteName != "" {
		return &ErrRemoteExists{Name: status.RemoteName, URL: status.RemoteURL}
	}
	var err error
	var remote *git.Remote
	remote, err = repo.Remote(DefaultP2PRemote)
	if err == git.ErrRemoteNotFound {
		var remoteConfig config.RemoteConfig
		remoteConfig.Name = DefaultP2PRemote
		if origin, err := repo.Remote(DefaultRemote); err == nil {
			remoteConfig.URLs = extractURLs(origin.Config().URLs[0])
		} else {
			// FIXME what to do here?
			// if we don't have an URL, we can't continue without manual provision,
			// so either instruct how to create remote, or ask for repo-url as parameter
			panic("Cannot derive url to use as repo ID for p2p:///")
		}
		_, err = repo.CreateRemote(&remoteConfig)
		return err
	} else if err != nil {
		return fmt.Errorf("failed to create remote for git-p2p: %v", err)
	} else if !FullRemoteURLPattern.MatchString(remote.Config().URLs[0]) {
		return fmt.Errorf("remote 'p2p' exists but with different remote URL format: %v", remote.Config().URLs[0])
	}
	return nil
}

var fullProtocolNotation = regexp.MustCompile(`[a-zA-Z+-_=]://(.*)`)
var shortProtocolNotation = regexp.MustCompile(`[a-zA-Z+-_=]:+(.*)`)

// TODO also drop query-part '?...' and client-based part '#...'?
// extractURLs extracts the hostname + port + path parts and drop the
// protocol-part of the URL.
func extractURLs(remoteURL string) []string {
	// FIXME support remote URL format: git@gitlab.com:cobratbq/git-p2p.git
	var matches []string
	if matches = fullProtocolNotation.FindStringSubmatch(remoteURL); matches != nil {
		url := P2PProtocolPrefix + matches[1]
		return []string{url}
	}
	if matches = shortProtocolNotation.FindStringSubmatch(remoteURL); matches != nil {
		url := P2PProtocolPrefix + matches[1]
		return []string{url}
	}
	panic("Unsupported remote URL format: " + remoteURL)
}
