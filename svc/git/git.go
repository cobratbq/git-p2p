package git

import (
	"io"
	"os/exec"
	"strings"

	"dannyvanheumen.nl/p2p/internal/util"
	"github.com/cobratbq/goutils/std/builtin"
)

// git service:
// - `git push p2p` exposes repository directory (including *all* references) at repoID as defined in remote URL.
// - `git fetch p2p` retrieves all references from the p2p network for repoID as defined in remote URL.
// - `git clone p2p://github.com/git/git.git` clones repo from repository known as repoID on p2p network.

// Service is the service implementation for the p2p git transport.
type Service struct {
	repos map[string]string
}

// NewService creates a new p2p-git service.
func NewService() *Service {
	return &Service{
		repos: make(map[string]string, 0),
	}
}

const gitReceivePack = "git-receive-pack"
const gitUploadPack = "git-upload-pack"
const discoverRepo = "p2p-discover-repo"

// HandleIPC handles local IPC connections.
func (s *Service) HandleIPC(conn io.ReadWriter, version uint64) {
	s.handle(conn, version)
}

// HandlePeer handles peer connections.
func (s *Service) HandlePeer(conn io.ReadWriter, version uint64) {
	// FIXME separate allowed peer actions, don't allow adding artibrary "repo location"s.
	s.handle(conn, version)
}

// Handle handles a git client connection, expecting to receive commands from
// `git-p2p`, the `git` p2p extension.
// FIXME require synchronization or should we leave it for now?
// FIXME implement status command in order to list repositories currently shared.
func (s *Service) handle(conn io.ReadWriter, version uint64) {
	builtin.Require(version == 0, "Only version 0 is supported.")
	util.Info("Start handling connection ...")

	// FIXME need to change to looking up path for repoID, instead of assuming repoID is directory path
	command, repoID, repoDir, err := readGitCommand(conn)
	if err != nil {
		util.Warn("Failed to acquire valid command: " + err.Error())
		return
	}

	switch command {
	case gitUploadPack:
		if location, ok := s.repos[repoID]; ok {
			uploadPack(conn, location)
		} else {
			util.Warn("RepoID " + repoID + " is unknown.")
			conn.Write([]byte("0000"))
		}
	case gitReceivePack:
		builtin.Require(repoDir != "-", "A 'git push' command should not be issued if we cannot find a repository.")
		if location, ok := s.repos[repoID]; ok {
			builtin.Require(repoDir == location, "We cannot handle two repositories with the same repository ID.")
			receivePack(conn, location)
		} else {
			// TODO needs synchronization
			util.Info("Registering " + repoID + ": " + repoDir)
			s.repos[repoID] = repoDir
			receivePack(conn, repoDir)
		}
	default:
		builtin.Unsupported("unsupported git command: " + command)
	}
}

func readGitCommand(conn io.ReadWriter) (string, string, string, error) {
	var commandline [255]byte
	n, err := conn.Read(commandline[:])
	if err != nil {
		util.Warn("Failed to retrieve command: " + err.Error())
		return "", "", "", err
	}
	parts := strings.SplitN(string(commandline[:n]), " ", 3)
	switch parts[0] {
	case gitUploadPack, gitReceivePack:
		return parts[0], parts[1], parts[2], nil
	default:
		panic("git: command '" + string(commandline[:n]) + "' is not whitelisted. Aborting.")
	}
}

func uploadPack(conn io.ReadWriter, location string) {
	// FIXME how can we determine what branch is pushed, such that we can expose that specific branch that was pushed?
	processPack(conn, gitUploadPack, location)
}

func receivePack(conn io.ReadWriter, location string) {
	processPack(conn, gitReceivePack, location)
}

func processPack(conn io.ReadWriter, command, location string) {
	cmd := exec.Command(command, location)
	cmdStdOut, err := cmd.StdoutPipe()
	if err != nil {
		util.Warn("Failed to acquire process stdout: " + err.Error())
		return
	}
	cmdStdIn, err := cmd.StdinPipe()
	if err != nil {
		util.Warn("Failed to acquire process stdin: " + err.Error())
		return
	}
	if err = cmd.Start(); err != nil {
		util.Warn("Failed to start git process: " + err.Error())
		return
	}

	go util.CopyNoWarning(cmdStdIn, conn)
	util.CopyWithWarning(conn, cmdStdOut)

	if err = cmd.Wait(); err != nil {
		util.Warn("Failed to finish git process: " + err.Error())
	}

	util.Info("Finished handling git connection.")
}
