package ipc

import (
	"errors"
	"fmt"
	"net"
	"os"
	"path"
	"regexp"

	"dannyvanheumen.nl/p2p/internal/util"
	"dannyvanheumen.nl/p2p/net/unixsocket"
	"dannyvanheumen.nl/p2p/protocol"
	"github.com/cobratbq/goutils/std/builtin"
)

const ipcAddressFormat = "p2p-ipc-%d"

// ServiceIDFormat specifies the format of the service identifier used for service=1 requests.
// TODO ServiceIDFormat should define desired format for service identifiers.
var ServiceIDFormat = regexp.MustCompile(`[a-zA-Z0-9]+:[a-zA-Z0-9]+=[0-9]+`)

// Establish - as an agent/server - a `p2p` IPC interface for clients to
// connect to.
func Establish() (net.Listener, error) {
	p2pIPCPath := generateName()
	listener, err := unixsocket.Listen(p2pIPCPath)
	if err != nil {
		return nil, errors.New("failed to bind to IPC address: " + err.Error())
	}
	return listener, nil
}

// ConnectService connects to the local P2P agent IPC and immediate issues a
// service pass-through to the service represented by the service identifier.
func ConnectService(serviceID string, peer string) (net.Conn, error) {
	builtin.Require(ServiceIDFormat.MatchString(serviceID), "Illegal service identifier used.")
	conn, _, err := Connect()
	if err != nil {
		return nil, err
	}
	// FIXME check existance of 'service=0' command before issuing.
	util.Debug("connecting to " + peer + " for service pass-through")
	if err := protocol.WriteCommand(conn, []byte(protocol.CmdService),
		[]byte(serviceID), []byte("remote="+peer)); err != nil {
		return nil, err
	}
	// FIXME does it make sense to even check beforehand whether or not a service exists? (maybe bad=0 is sufficient indication)
	confirmation, err := protocol.ReadCommand(conn)
	if err != nil {
		return nil, err
	}
	if string(confirmation) != protocol.ResponseOk {
		return nil, os.ErrNotExist
	}
	return conn, nil
}

// Connect - as a client - to an active `p2p` IPC interface.
func Connect() (net.Conn, *protocol.Announcement, error) {
	p2pIPCPath := generateName()
	conn, err := unixsocket.Dial(p2pIPCPath)
	if err != nil {
		return nil, nil, errors.New("failed to establish connection with P2P agent: " + err.Error())
	}
	ann, err := protocol.ReadAnnouncement(conn)
	if err != nil {
		return nil, nil, err
	}
	return conn, &ann, nil
}

func generateName() string {
	return path.Join(os.TempDir(), fmt.Sprintf(ipcAddressFormat, os.Getuid()))
}
