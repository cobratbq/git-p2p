module dannyvanheumen.nl/p2p

go 1.13

require (
	github.com/cobratbq/goutils v0.0.0-20200203011821-a30c33bf8fb6
	github.com/google/go-cmp v0.3.1 // indirect
	github.com/juju/fslock v0.0.0-20160525022230-4d5c94c67b4b
	github.com/klauspost/cpuid v1.2.3 // indirect
	github.com/klauspost/reedsolomon v1.9.3 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/stretchr/testify v1.4.0 // indirect
	github.com/templexxx/cpufeat v0.0.0-20180724012125-cef66df7f161 // indirect
	github.com/templexxx/xor v0.0.0-20191217153810-f85b25db303b // indirect
	github.com/tjfoc/gmsm v1.2.0 // indirect
	github.com/xtaci/kcp-go v5.4.20+incompatible
	github.com/xtaci/lossyconn v0.0.0-20190602105132-8df528c0c9ae // indirect
	github.com/xtaci/smux v1.5.9
	golang.org/x/crypto v0.0.0-20200128174031-69ecbb4d6d5d // indirect
	golang.org/x/net v0.0.0-20200202094626-16171245cfb2 // indirect
	golang.org/x/sys v0.0.0-20200202164722-d101bd2416d5 // indirect
	gopkg.in/src-d/go-git.v4 v4.13.1
)

replace github.com/xtaci/kcp-go => ../kcp-go
