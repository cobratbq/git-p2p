# Design: P2P

A general-purpose peer-2-peer transport layer.

__Primary goals__: _collaboration platform, simple and powerful_

- Safety: `TODO: safe/confidential/... detail requirements/concerns`:
- Reliability, stability, simplicity: effortless set-up and use of p2p transport  
  _mediator/transport/entry-point for peer services, peers advertise services such as `git-p2p`_
- Networking/transport:
  - operates on the local subnet
    - _Assumption_: local subnet contains unfriendly clients
    - _Assumption_: p2p network may have unfair/unfriendly peers  
      `TODO: within reason, specify in more detail what concerns we desire to prevent/mitigate`
  - handles connections from/over the internet
    - though not with automatic discovery.
- _p2p_ management provides information on/connection to other peers.  
  _NOTE: p2p expects the application itself to establish a connection to the peer for actual services._

⚠ Until 1.0 release, nothing in the implementation is considered fixed ⚠

## Decisions

- Single-command way-of-working. The 'single command' may be connection multiplexing, then new stream accept handler starts and this allows for multiple connections established over single connection.
- End-to-end connection characteristics for underlying transport protocol:
  - reliable
  - ordered
  - connectionless  
    _Improved compatibility with routers/NAPT/etc compared to TCP._
  - `TODO: ...`
- P2P-network characteristics
  - Always connected  
    Set up peer connections with other peers irrespective of whether they provide useful services. (_p2p_ network can provide multiple services.)
  - Discoverable/discovery:
    - Sharing of unknown peers through connections with known peers.
    - Discovery of peers on the network through mDNS service discovery.
- Minimum set of commands for `p2p` network management.
  - Register new peer in the network. (Respond with address new peer is known by the network.)
- Every peer has a persistent identifier.
  - Long-term to ensure that references to the peer don't go stale.
  - Cryptographically secure identity, to prevent impersonation.
  - ...
- Ability to define _extensions_ that define commands for a specific purpose, e.g. `git-p2p`.
- Ability to issue on-demand connection to address to facilitate punching UDP-hole in firewall/NAT.
- Interfaces:
  - Local RPC interface for user/OS for network management commands.
    - Transport: unix socket at `/tmp/p2p-rpc-<uid>.sock`, guarded by `/tmp/p2p-rpc-<uid>.lock` file-based mutex.
    - Predefined RPC interface (e.g. JSON-RPC)
  - Remote interface for peer connections.
    - Define trivial interface that allows for extensibility and elevation to application-level protocols, such as `git-p2p`. (Simplicity similar to _Wireguard_.)
- Ensure single `p2p` instance that manages the peer-2-peer network for the user.
- Dual-mode interaction:
  1. _p2p_ management interface
  1. application-level transparent "pass-through" (encapsulation mode similar to `CONNECT` method for HTTP proxies)
- The `p2p` management process does not borrow out its connection to client applications. Client applications are expected to start their own connection to the desired peer.

## Assumptions

- Punching holes through firewall/NAT:
  - Given that mDNS discovery works on both sides: both sides will be trying to connect with the other side, which should open up the UDP ports.
  - Allow issuing manual connect commands with specified address, both parties can do that to "force the connection".
  - Upon receiving initial packet from new peer, inspect source address and attempt connection on that address (assuming it is unknown) This allows detection of remapping of UDP ports not known to originating client.
- Each peer can directly reach each other peer. (No need for proxying, gateways etc.)

## Specification

The specification for `p2p` as it develops.

- [P2P agent local IPC](ipc.md)

## TODO

- Need a DHT to ensure shared data is synchronized properly?
- Define trivial top-level RPC language akin Wireguard's `get=1` and `set=1` for "_1st_ version/implementation of _get_ operation".
  - trivially extensible
  - each operation *may* define its own payload format (allows trivially transitioning to other RPC languages and allows selecting basic syntax for trivial operations, while leveraging complicated formats for complex operations)
- Do we need multiplexing, multiple streams over single connection?
- Define format for _extension commands_, such as `git-p2p`.
- Determine which RPC protocol for local management connections.
- Specify command for passing control onto the _extension_, for given session/stream. (stream - assuming multiplexing)
- Need some kind of heart beat or ping to keep the UDP pass-through open on firewall/NAT?
- Define security concerns/decisions.
- Does it make sense to send back the (UDP) network address as the client is known to its peers?
- Do we need to feed back the client's addressed as known by its peers such that the `p2p` client becomes aware of UDP port remapping, e.g. by routers/modems. (Shouldn't be necessary as a primary concern, as it should imply use over subnets, instead of locally.)
- QUIC mature enough that it we can benefit from adopting QUIC as opposed to KCP for inter-peer-communicaton?
- Out-of-process extensions? (connected over stdin/stdout? multiplexing for multiple accesses?)

## Future research/use

One can imagine other uses for a reliable, self-sustaining peer-2-peer network. There are - as of yet - no intentions to work on these applications.

- Notifications for OS/application updates
- Propagation of OS/application updates themselves
- Security profiles:
  - Profile: open - access to all services hosted on p2p network.
  - Profile: authentication - after successful authentication, access to all services hosted on p2p network.
  - Profile: authorization - after successful authentication, controlled access to services.
- Need to distinguish between subnets:
  - by default P2P to handle each subnet separately.
  - option to be gateway to connect p2p networks/redirect out-of-subnet service into local subnet/p2p network?
  - ...
- Client-applications for P2P platform:
  - git remote transport
  - one-to-one chat application
    - send messages to other peer: simple, accessible, readily available.
    - rely on unicode for smileys etc.
  - topcs-based chat channels (a la IRC channels/twitter hashtags)
    - only suitable for ad-hoc conversations, not for large-scale persistent discussions.
    - creator becomes "topic server"
    - builds/relies on one-to-one chat application infrastructure?
  - file transfer
    - simple, accessible, readily available.
    - offer single file at a time, by _name_ + _size_.
    - only upload if offer is accepted.
    - transport is confidential.
- ...

## References

- Discovery/Announcement
  - [Multicast UDP](https://en.wikipedia.org/wiki/IP_multicast)
    - [Cisco: IP Multicast Deployment Fundamentals](https://www.cisco.com/en/US/tech/tk828/tech_brief09186a00800e9952.html#wp17550)
  - [mDNS](https://en.wikipedia.org/wiki/Multicast_DNS) (Avahi, Bonjour)
    - [DNS-SD](https://en.wikipedia.org/wiki/Zero-configuration_networking#DNS-SD)
  - Librecast (???)
- Networking
  - Transport-protocols:
    - [UDP](https://en.wikipedia.org/wiki/User_Datagram_Protocol)
    - [Wind][wind-project] - Off-Grid Services for Everyday People [Concept video][wind-concept-video], [35C3][35C3-wind]
    - [KCP][kcp] ([kcp-go])
    - [QUIC](https://en.wikipedia.org/wiki/QUIC "Wikipedia: QUIC") ([QUIC working group](https://github.com/quicwg), [quic-go](https://github.com/lucas-clemente/quic-go))
  - Multiplexing:
    - [SMUX](https://github.com/xtaci/smux) - muxing protocol for use on top of KCP, if necessary.  
      _Recommended by KCP, as KCP itself does not provide connection maintenance/keepalive/heartbeats._
    - [Yamux](https://github.com/hashicorp/yamux)

<!------------------------------------------------------------------------>

[wind-project]: https://guardianproject.info/wind/ "Wind: Off-Grid Services for Everyday People"
[35C3-wind]: https://media.ccc.de/v/35c3-9595-wind_off-grid_services_for_everyday_people "Wind: Off-Grid Services for Everyday People"
[wind-concept-video]: https://www.youtube.com/watch?v=fGuiy3rlOVQ&feature=youtu.be "Wind project: Prototype Concept Video"
[kcp]: https://github.com/skywind3000/kcp "KCP - A Fast and Reliable ARQ Protocol"
[kcp-go]: https://github.com/xtaci/kcp-go "KCP-GO: A Crypto-Secure, Production-Grade Reliable-UDP Library for golang with FEC"
