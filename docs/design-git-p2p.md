# Design: git-p2p

A `git` transport protocol for establishing remote connections over `TODO:p2p` network.

__Primary goals__: ...

- frictionless collaboration  
  _transparent, virtually invisible_
- effortless set-up  
  _hide complexity, provide necessary information_
- reliable in use  
  _works as expected, works when expected_

⚠ Until 1.0 release, nothing in the implementation is considered fixed ⚠

## Decisions

```TODO
2 variants/levels of interaction possible:
- specific remote: p2p://<fingerprint>@github.com/cobratbq/bla.git, indicates specific remote on P2P network.
- full P2P net: p2p://github.com/cobratbq/bla.git, indicates all currently available remotes.
```

- `p2p://` (`git-remote-p2p`) name of the git transport for remote connections.
- `git p2p` (`git-p2p`) the command for managing the _p2p_ transport for matters concerning _git_ transport.
- `p2p` the default _remote_ name for remotes configured to use the `p2p://` transport.  
  `p2p://github.com/user/repo.git` where:
  - `p2p://` indicates use of _git-remote-p2p_ transport.
  - `github.com/user/repo.git` indicates the identifier of the repository hosted on the p2p network, similar to the original repository location except over a different transport protocol.
    - By default, take repository id from `origin` remote. (Document in `git p2p` command help.)
- References acquired through _p2p_ transport:
  - format `remotes/p2p/<peer-id>/master`, where `<peer-id>` is _p2p network_ peer identifier.
  - `<peer-id>` allows distinguishing between references from different peers on the p2p network.
  - ...
- _git_ config prefix `git-p2p.` for _git p2p_ configuration entries.
  - `git-p2p.default-remote-name` to configure default name for the _p2p_ remote.
- the identifier for a git repository on the p2p network. Advice:
  - if already exposed either publicly or privately: _original repository url_  
    easily predictable so likely common among participants
  - if not yet exposed: use an arbitrary name or use a _likely future repository url_
- Security:
  - do not allow listing of repositories available at peers  
    As we attempt to make repository ids predictable, by not listing we avoid "uninvited browsing for unknown repositories". This is not intended as a security feature, but for privacy.  
    _Assumption_: if a user knows a given repository, the repo id should be predictable. If the user does not know of a repository, it should not matter that the repository (or its repo id) is not predictable or discoverable.

## Commands

- _git-p2p passthrough_ for _git_ transport traffic to be passed through uninterpreted to _git_ client.

## `git` commands

- `git clone` clone a repository published on the p2p network
- `git fetch` acquire new references from the p2p network
- `git push` publish new references onto the p2p network (This may be used to expose individual references onto the p2p network, giving users careful control of what is shared over the p2p network.)

Any of these `git` commands will start a _p2p_ server if not yet running.

## _git p2p_ (`git-p2p`) command

- Help/documentation on usage of _git p2p_.
  - How to get started.
  - Initial analysis of existing `p2p` set-up. Clarify what to do to get started.
  - Offer more extensive documentation using `--help`.
- Initial set-up through cloning from source on the p2p network.
- Initial set-up borrowing from `origin` remote for repo identifier.

## `git-remote-p2p` commands

The program that _git_ uses for any interaction with the _p2p_ protocol.

- `p2p://github.com/me/my-project.git` (`git-remote-p2p`) support process for handling interaction with the `p2p` protocol when used in a remote.

`git-remote-p2p` follows the [git-remote-helpers][git-remote-helpers] protocol.

## Outline

First-time set-up:

1. Clone a new repository from p2p: `git clone p2p://my-project-uri/project.git`, or:  
Set-up remote `p2p` with URL based on protocol `p2p://<URI>`.

Each time a p2p network needs to be established/updated:

1. `git fetch p2p`,  
   `git push p2p`

Done. You can start interacting with the p2p network using typical git commands.

## TODO

- Can/should we do rudimentary checks to ensure that we do not expose non-git directories?

## References

- Git
  - [gitremote-helpers]
  - [git-remote-testgit]
  - [git-remote-fd]
  - [git-remote-ext]
  - [Git Hooks][git-hooks]
- Chapter 10: Git Internals
  1. [Plumbing and Porcelain][gitinternals-plumbing]
  1. [Git Objects][gitinternals-git-objects]
  1. [Git References][gitinternals-git-references]
  1. [Packfiles][gitinternals-packfiles]
  1. [The Refspec][gitinternals-the-refspec]
  1. [Transfer Protocols][gitinternals-transfer-protocols]
  1. [Maintenance and Data Recovery][gitinternals-maintenance-and-data-recovery]
  1. [Environment Variables][gitinternals-environment-variables]
  1. [Summary][gitinternals-summary]
- [Git remote helpers](https://github.com/git/git/blob/master/Documentation/gitremote-helpers.txt)
- [Git wire protocol version 2](https://git-scm.com/docs/protocol-v2)
- [Git capabilities protocol version 1](https://github.com/git/git/blob/master/Documentation/technical/protocol-capabilities.txt)
- [Git capabilities protocol version 2](https://github.com/git/git/blob/master/Documentation/technical/protocol-v2.txt)
- [Git pack protocol](https://github.com/git/git/blob/master/Documentation/technical/pack-protocol.txt)
- [Git protocol common](https://github.com/git/git/blob/master/Documentation/technical/protocol-common.txt)
- [git-remote-ipfs](https://github.com/cryptix/git-remote-ipfs) implementation of git-remote communication protocol

<!------------------------------------------------------------------------>

[git-hooks]: https://git-scm.com/book/en/v2/Customizing-Git-Git-Hooks "Git Hooks"
<!-- Previously: https://git-scm.com/docs/git-remote-helpers -->
[gitremote-helpers]: https://git-scm.com/docs/gitremote-helpers "git: Helper programs to interact with remote repositories"
[git-remote-testgit]: https://git-scm.com/docs/git-remote-testgit "remote-testgit example"
[git-remote-fd]: https://www.git-scm.com/docs/git-remote-fd "git-remote-fd - Reflect smart transport stream back to caller"
[git-remote-ext]: https://www.git-scm.com/docs/git-remote-ext "git-remote-ext - Bridge smart transport to external command"
[gitinternals-plumbing]: https://git-scm.com/book/en/v2/Git-Internals-Plumbing-and-Porcelain "Git Internals - Plumbing and Porcelain"
[gitinternals-git-objects]: https://git-scm.com/book/en/v2/Git-Internals-Git-Objects "Git Internals - Git Objects"
[gitinternals-git-references]: https://git-scm.com/book/en/v2/Git-Internals-Git-References "Git Internals - Git References"
[gitinternals-packfiles]: https://git-scm.com/book/en/v2/Git-Internals-Packfiles "Git Internals - Packfiles"
[gitinternals-the-refspec]: https://git-scm.com/book/en/v2/Git-Internals-The-Refspec "Git Internals - The Refspec"
[gitinternals-transfer-protocols]: https://git-scm.com/book/en/v2/Git-Internals-Transfer-Protocols "Git Internals - Transfer Protocols"
[gitinternals-environment-variables]: https://git-scm.com/book/en/v2/Git-Internals-Environment-Variables "Git Internals - Environment Variables"
[gitinternals-maintenance-and-data-recovery]: https://git-scm.com/book/en/v2/Git-Internals-Maintenance-and-Data-Recovery "Git Internals - Maintenance and Data Recovery"
[gitinternals-summary]: https://git-scm.com/book/en/v2/Git-Internals-Summary "Git Internals - Summary"
