# Requirements

1. `p2p` transport network is self-sustaining as long as there is a connection to one other peer.
   - connection with other peer proves that communication is possible
   - peer discovery works at minimum when an existing network is extended
   - when a connection was originally established, connection repair can be initiated from two sides
1. Fully idiomatic with `git`'s existing mechanisms.  
   _No shortcuts, no deviation from expectations._
1. Automated, given consistent configuration.  
   _No complex set-up. No unnecessary, complicated questions. Clear error messages upon discovery of inconsistency._
1. Security:
   - data security: local extensions determine all access to the machine past the p2p-agent. (remote side can only do requests)
   - identity (persistent): cryptographically secure long-term identity
     - reliable in dynamically changing p2p-nets / over network interruptions / ...
     - reliable/dependable identifier for relying services to use when referring to other peers (git refs, chat messages, presence, file transfer, ...)
   - `TODO: authentication, based on long-term identity, but needs thinking and probably further work`
   - `TODO: authorization urgently needed? based on long-term identity, use as SSH authorized-keys list? Do we need more? Should we want more?`
   - `TODO: confidentiality, to be determined at transport-level, probably relying on underlying protocol capabilities, such as KCP/QUIC/...`
1. Sane defaults:
   - sufficiently secure by default,
   - right configuration by default,
   - minimal configuration by default,
   - ...
   - select defaults for ease-of-use, as long as we do not compromise other requirements
1. Clean separation between `p2p` the transport layer and `git-p2p` the plug-in that leverages the transport for _git_ traffic.
