# File Transfer

File transfer over P2P network connection.

## Commands

- `offer=0` (IPC/Peer), followed by properties `name`, `length`, `type` in order.  
  Offer content for transfer:
  - C: `offer=0\nname=<filename>\nlength=<length-in-bytes>\ntype=<content-type-string>\n\n`
  - S: prepares for reception, e.g. allocate resources/destination file, then confirms with `ok=1`.
  - C: Send content `<content-bytes><sha256-checksum-bytes>` and closes connection.
- `receive=0` (IPC only)  
  Establishes multiplexed connection, then awaits transfer requests `offer=0`. May receive many file transfers concurrently.
