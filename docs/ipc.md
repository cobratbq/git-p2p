# IPC

API for interaction with local agent.

## Interface

The agent establishes a unix domain socket connection at: `/tmp/p2p-ipc-$(id -u).sock` with a file system lock at `/tmp/p2p-ipc-$(id -u).lock`. This ensures that only at most one agent is active for a user at any given time.

## Commands

`FIXME: check if 255 is still the correct max command length.`

A top-level command takes form: `<name>=<version>\n` with maximum length of `255` bytes. A `name` is one of a predefined set of command names. `version` is a number indicating a version. In command listings, `\n` is left out for conciseness. The rule, without exception, is that any command is finalized with `\n`.

Version `1` is considered stable, with no expectation to change. Any subsequent changes will stabilize into a new version increment. Version `0` can be considered "in active development" with no guarantees.

Version numbers do not indicate preference or obsolescense. The versions are distinguishers for different execution semantics and/or result formats. This includes: protocol/mechanism, configuration parameters, response format. For a single version, there must not be any ambiguity.

### Common

- `service=0`, followed with `<groupID>:<serviceID>=<version>`,  
  response: `ok=1` for success, `TODO: specify failure response` for failure.  
  Request transparent pass-through to service with identifier `<groupID>:<serviceID>` having version `<version>`.  
  Note that, as with top-level commands, a pair `<groupID>:<serviceID>=<version>` represents a fixed, unambiguous set of interactions. Knowing what `version` of a service you request, implies knowing all possible follow-up commands, such that there can be no ambiguity or potential for protocol violations.

### IPC (local)

- `connect=0` request local P2P agent to establish a connection with a specified remote peer.
- `status=0`, JSON-encoded status information  
  Returns status report for p2p agent.
- `mux=0`, request connection multiplexing for a specific local IPC connection.

### Peers (remote)

- `register=0` register a (remote) P2P agent for a persistent connection. Persistent connections are always, automatically - that is, as part of the registration - multiplexed.

### Responses

`FIXME: check if all are needed.`

- `ok=1`, no response body  
  Response signaling successful (acceptance of) command. (Blocking: send after full execution completed.)
- `bad=0`, no response body `TODO: ???? is this any good? Use 'ng=1'? We need something to reject commands from remote.`  
  Response signaling rejection of command due to failure at side of sender.
- `np=0`, no response body  
  Response signaling unsuccessful command due to receiving side not being prepared to execute it.
- `fail=0`, no response body  
  Response signaling unexpected server failure, hence command is aborted.

`TODO: document services, transparent pass-through, service IDs.`

## Follow-up on commands

In case requester needs to follow up with additional information, data can immediately be sent after the newline (`\n`) byte. In case we expect the responder to follow up first, start reading for response data.

`FIXME: consider dumping allowed commands upon connecting to server. Then we can immediately disconnect on protocol violation and above behavior can never be a problem.`

## Service pass-through

A service identifier takes form: `<groupID>:<serviceID>=<version>` with maximum length of `255` bytes.

- `dannyvanheumen.nl:git=0` to pass through to git-p2p network peer.
