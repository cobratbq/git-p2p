# Notes

Arbitrary notes that haven't gotten a place yet.

## Announcement

`<protocol-identifier:2-bytes-LE-unsigned-int>id=<peer-identity> commands=<comma-separated-commands-list>\n`

- `protocol-identifier:2-bytes-LE-unsigned-int` a protocol identifier through an unsigned integer value that indicates the whole further rest of the communication protocol. By reading 2 bytes, one can fully determine what protocol is active.
  - `0x0000`: in-development protocol version
    - announcement string consists of key-value entries separated by spaces.
- `id=<peer-identity>`: peer-identity represented as hexadecimal characters.
- `commands=<comma-separated-commands-list>`: list of available commands, separated by commas.
  - Multiple commands where only version is different, may occur. A command takes format `commandname=version`, with:
  - `commandname` is name, using alpha characters,
  - `version` indicates variant of the command, by numeric value in hexadecimal representation.
