# Open issues

## Questions

- investigate security concerns, defaults (encryption, privacy, authn/authz, etc.)
- do we need aliasing for the long-term identifiers of clients, such that git references become more accessible?
- revisit decisions
- Given clients have/need a long-term identity:
  - is it used to identify the user (owner) or the client (machine)?
  - what expectations do we have for this identity?
    - authentication, i.e. prevent impersonation
- Obfuscate repository names by taking a hash (or something similar) for future cases where one peer proxies for other peers?
- P2P-proxy as transport-level function that transparently services all extensions?
- Need for some sort of heartbeat to keep connections/UDP port mapping alive?
- Urgency of authn/authz? (As in, is it relevant to the most significant use cases? Are the use cases where it is import relevant to p2p?)
- client-identifier: long-term identity for (reuse) in p2p connections.
  - client-identifier can be used for `git-p2p` as identifier-component in references.
- single p2p management process per user w/ rpc for interop/management?
- What modes available? (open, authn/authz, ...)
- What security/privacy concerns?
- expose single or all branches?
- allow querying for repos?
