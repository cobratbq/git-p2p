# Coding conventions

## Conventions

1. For _unexported_ types, use exported functions (capitalized) to indicate access allowed from other code in same package. Unexported functions only allowed for logic on same type.
   - Unexported field: only accessible to type logic.
   - Exported field: accessible to package logic.