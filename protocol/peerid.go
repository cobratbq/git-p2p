package protocol

import (
	"encoding/hex"
	"os"
)

// identifierLength is the length of the peer identifier.
const identifierLength = 20

// PeerID is the type containing the unique peer identifier.
type PeerID [identifierLength]byte

// ParsePeerID parses a byte-array and extracts a Peer ID given all conditions
// are met, or defaults to the default Peer ID value if missing (i.e.
// completely empty).
// In case of "empty" peer ID, default to the provided default value.
func ParsePeerID(value []byte, def PeerID) (PeerID, error) {
	var peer PeerID
	if string(value) == "" {
		peer = def
	} else {
		n, err := hex.Decode(peer[:], value)
		if err != nil || n != identifierLength {
			return PeerID{}, os.ErrInvalid
		}
	}
	return peer, nil
}
