package protocol

import (
	"bytes"
	"encoding/hex"
	"io"
	"os"
	"strings"

	"github.com/cobratbq/goutils/std/builtin"
)

const (
	// CmdConnect represents connect request.
	CmdConnect = "connect=0"
	// CmdMux represents connection multiplexing request.
	CmdMux = "mux=0"
	// CmdRegister represents peer registration request.
	CmdRegister = "register=0"
	// CmdStatus represents request for status information.
	CmdStatus = "status=0"
	// CmdService represents service pass-through request.
	CmdService = "service=0"

	// ResponseOk indicates positive confirmation (without payload)
	ResponseOk = "ok=1"
	// ResponseBad indicates negative confirmation due to bad request (request-side)
	// FIXME consider defining variant with payload describing violation.
	ResponseBad = "bad=0"
	// ResponseFail indicates negative confirmation due to unexpected failure (response-side)
	// FIXME consider defining variant with payload describing possible response-side issues.
	ResponseFail = "fail=0"
)

const (
	// FIXME re-evaluate arbitrarily chosen max length for announcement. (Do we even care about a reasonable max?)
	maxAnnouncementLength = 8192
	// TODO reconsider choice of maximum length for command. May be important if we take this as general IPC standard.
	maxCommandLength        = 255
	initialCommandsListSize = 5
)

// ReadAnnouncement reads capability announcement line.
// communicate: peer ID, supported commands.
// DO NOT communicate: services, i.e. allow list to be dynamic.
// FIXME call them 'commands' or 'operations'? Possibly shortened to 'ops' in announcement.
func ReadAnnouncement(in io.Reader) (Announcement, error) {
	var version [2]byte
	n, err := in.Read(version[:])
	if err != nil {
		return Announcement{}, err
	}
	if n != 2 || version != [2]byte{0, 0} {
		return Announcement{}, os.ErrInvalid
	}

	// Process capability announcement for protocol version 0x0000.
	var line [maxAnnouncementLength]byte
	n, err = ReadLine(line[:], in)
	builtin.RequireSuccess(err, "failed to read (full) announcement line: %+v")
	var ann = Announcement{}
	for _, e := range bytes.Split(line[:n], []byte{' '}) {
		switch {
		case bytes.HasPrefix(e, []byte("id=")):
			hex.Decode(ann.ID[:], e[3:])
		case bytes.HasPrefix(e, []byte("commands=")):
			ann.Commands = make(map[string]struct{}, initialCommandsListSize)
			for _, e := range bytes.Split(e[9:], []byte{','}) {
				ann.Commands[string(e)] = struct{}{}
			}
		default:
			return Announcement{}, os.ErrInvalid
		}
	}
	return ann, ann.Validate()
}

// ZeroID is the all-zero peer identifier.
var ZeroID = PeerID{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}

// Announcement contains the capability announcement.
// FIXME identity to include "scheme"? (id:0123456789abcdef0123456789abcdef12345678)
type Announcement struct {
	ID       PeerID
	Commands map[string]struct{}
}

// Validate validates announcement content.
// TODO consider allowing 0 commands, no ID, such that announcement can be used for immediately negotiating protocol "upgrade". (e.g. to upgrade to binary protocol, small announcement beneficial)
func (a *Announcement) Validate() error {
	if a.ID == ZeroID || len(a.Commands) == 0 {
		return os.ErrInvalid
	}
	return nil
}

// ReadCommand reads a single command (newline-terminated) from the
// IPC-connection.
func ReadCommand(in io.Reader) ([]byte, error) {
	var line [maxCommandLength]byte
	n, err := ReadLine(line[:], in)
	return line[:n], err
}

// ReadLine reads a single line from the connection.
func ReadLine(line []byte, in io.Reader) (int, error) {
	var b [1]byte
	for i := 0; i < len(line); i++ {
		if n, err := in.Read(b[:]); err != nil || n == 0 {
			return 0, io.ErrUnexpectedEOF
		}
		if b[0] == '\n' {
			return i, nil
		}
		line[i] = b[0]
	}
	return 0, io.ErrShortBuffer
}

// WriteAnnouncement writes capability announcement line.
// TODO should announcement include all supported protocol versions, such that protocol "upgrades" are possible? E.g. I'm talking 0x0000, but I also do 0x0001 and 0x0002.
func WriteAnnouncement(out io.Writer, ID []byte, command ...string) error {
	line := []byte{0, 0}
	line = append(line, []byte("id=")...)
	encodedID := make([]byte, hex.EncodedLen(len(ID)))
	hex.Encode(encodedID, ID)
	line = append(line, encodedID...)
	line = append(line, ' ')
	line = append(line, []byte("commands=")...)
	line = append(line, []byte(strings.Join(command, ","))...)
	return WriteLine(out, line)
}

// WriteCommand writes provided commands.
func WriteCommand(out io.Writer, command ...[]byte) error {
	// FIXME can we fix to 1024 chars max? This isn't good code, right now ...
	// message := make([]byte, 0, 1024)
	return WriteLine(out, bytes.Join(command, []byte{'\n'}))
}

// WriteLine writes out a line and finishes with newline character.
func WriteLine(out io.Writer, line []byte) error {
	_, err := out.Write(append(line, '\n'))
	return err
}

// Expect checks for error and either panics on error, or passes through result.
func Expect(result []byte, err error) []byte {
	builtin.RequireSuccess(err, "unexpected failure encountered")
	return result
}
