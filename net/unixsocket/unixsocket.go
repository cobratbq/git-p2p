package unixsocket

import (
	"net"
	"os"

	"github.com/cobratbq/goutils/std/io"
	"github.com/juju/fslock"
)

const network = "unix"

// Listener is a struct that combines a net.UnixListener together with the
// corresponding file lock as a structure. This allows protecting the unix
// sockets from access by multiple servers.
type Listener struct {
	lock     *fslock.Lock
	listener *net.UnixListener
}

// Listen binds to a unix socket to listen for incoming client connections.
func Listen(addr string) (*Listener, error) {
	var err error
	var sockaddr *net.UnixAddr
	if sockaddr, err = net.ResolveUnixAddr(network, addr+".sock"); err != nil {
		return nil, err
	}
	lock := fslock.New(addr + ".lock")
	if err := lock.TryLock(); err != nil {
		return nil, err
	}
	os.Remove(sockaddr.Name)
	var listener *net.UnixListener
	if listener, err = net.ListenUnix(network, sockaddr); err != nil {
		lock.Unlock()
		return nil, err
	}
	return &Listener{lock: lock, listener: listener}, nil
}

// Accept accepts an incoming client connection.
func (l *Listener) Accept() (net.Conn, error) {
	return l.listener.Accept()
}

// Addr returns the address of a listener.
func (l *Listener) Addr() net.Addr {
	return l.listener.Addr()
}

// Close closes a listener, and releases the corresponding file lock.
func (l *Listener) Close() error {
	io.CloseLogged(l.listener, "failed to close listener: %+v")
	if err := l.lock.Unlock(); err != nil {
		return err
	}
	return nil
}

// Dial connects a client to a listening unix socket interface.
func Dial(addr string) (*net.UnixConn, error) {
	var err error
	var sockaddr *net.UnixAddr
	if sockaddr, err = net.ResolveUnixAddr(network, addr+".sock"); err != nil {
		return nil, err
	}
	return net.DialUnix(network, nil, sockaddr)
}
