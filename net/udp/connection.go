package udp

import (
	"net"

	"github.com/xtaci/kcp-go"
)

const (
	dataShards   = 0
	parityShards = 0
)

// TODO configure encryption once basic functionality is working.
var blockCrypt kcp.BlockCrypt = nil

type Listener struct {
	listener *kcp.Listener
}

// Listen on specified address for incoming peer connections.
func Listen(conn *net.UDPConn) (*Listener, error) {
	l, err := kcp.ServeConn(blockCrypt, dataShards, parityShards, conn)
	if err != nil {
		return nil, err
	}
	return &Listener{listener: l}, nil
}

// Session represents a peer session.
type Session struct {
	session *kcp.UDPSession
}

// Accept accepts an incoming client connection.
func (l *Listener) Accept() (*Session, error) {
	conn, err := l.listener.AcceptKCP()
	if err != nil {
		return nil, err
	}
	configure(conn)
	return &Session{session: conn}, nil
}

func (l *Listener) Addr() net.Addr {
	return l.listener.Addr()
}

func (l *Listener) Close() error {
	return l.listener.Close()
}

// Dial connects to a listening server connection.
// FIXME how to detect lost connection instead of waiting infinitely?
func (l *Listener) Dial(remote string) (*Session, error) {
	remoteAddr, err := net.ResolveUDPAddr("udp", remote)
	if err != nil {
		return nil, err
	}
	peerConn, err := kcp.NewConn4(l.listener, remoteAddr)
	if err != nil {
		return nil, err
	}
	configure(peerConn)
	return &Session{session: peerConn}, nil
}

// RemoteAddr returns the remote address of a session.
func (s *Session) RemoteAddr() net.Addr {
	return s.session.RemoteAddr()
}

// ID returns a connection ID.
func (s *Session) ID() uint32 {
	return s.session.GetConv()
}

// Read reads data from a connection.
func (s *Session) Read(b []byte) (int, error) {
	return s.session.Read(b)
}

// Write writes data to a connection.
func (s *Session) Write(b []byte) (int, error) {
	return s.session.Write(b)
}

// Close closes an existing connection.
func (s *Session) Close() error {
	return s.session.Close()
}

func configure(conn *kcp.UDPSession) {
	conn.SetWriteDelay(false)
	conn.SetStreamMode(true)
	conn.SetACKNoDelay(true)
}
