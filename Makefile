
.PHONY: all
all: library p2p git-p2p transferp2p

.PHONY: library
library:
	go build ./...

.PHONY: p2p
p2p: library
	go build ./cmd/p2p

.PHONY: git-p2p
git-p2p: library
	go build ./cmd/git-p2p

.PHONY: transferp2p
transferp2p: library
	go build ./cmd/transferp2p

.PHONY: clean
clean:
	rm -f p2p git-p2p transferp2p
